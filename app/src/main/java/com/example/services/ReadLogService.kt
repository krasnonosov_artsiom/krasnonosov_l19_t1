package com.example.services

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.example.utils.FileHandler

class ReadLogService : Service(){

    private val binder: IBinder = ReaderBinder()
    private val fileHandler = FileHandler()

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    fun readLogs() : String {
        return fileHandler.readLogFile()
    }

    class ReaderBinder : Binder() {
        fun getService() : ReadLogService{
            return ReadLogService()
        }
    }
}