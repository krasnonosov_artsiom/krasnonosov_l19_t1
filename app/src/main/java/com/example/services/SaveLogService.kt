package com.example.services

import android.app.IntentService
import android.content.Intent
import android.widget.Toast
import com.example.utils.FileHandler
import com.example.utils.KEY_LOG

class SaveLogService : IntentService("WriterService") {

    private val fileHandler = FileHandler()

    override fun onHandleIntent(intent: Intent?) {
        val log: String? = intent?.getStringExtra(KEY_LOG)
        fileHandler.writeLogInFile(log!!)
        Toast.makeText(applicationContext, "Have written", Toast.LENGTH_SHORT).show()
    }
}