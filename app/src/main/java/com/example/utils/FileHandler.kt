package com.example.utils

import java.io.*

class FileHandler {

    private val file = File("$PATH_TO_FILE/Log.txt")

    fun writeLogInFile(log: String) {
        File(PATH_TO_FILE).mkdir()
        val bufferedWriter = BufferedWriter(FileWriter(file, true))
        bufferedWriter.write(log)
        bufferedWriter.close()
    }

    fun readLogFile(): String {
        if (!file.exists()) {
            return "Nothing to read!"
        }
        val bufferedReader = BufferedReader(FileReader("$PATH_TO_FILE/Log.txt"))
        return bufferedReader.readText()
    }
}