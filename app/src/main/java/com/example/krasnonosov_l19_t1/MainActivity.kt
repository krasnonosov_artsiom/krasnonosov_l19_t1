package com.example.krasnonosov_l19_t1

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.services.ReadLogService
import com.example.services.SaveLogService
import com.example.utils.KEY_LOG
import com.example.utils.KEY_TEXT_VIEW
import kotlinx.android.synthetic.main.activity_main.*

const val PERMISSION_REQUEST_CODE = 101

class MainActivity : AppCompatActivity() {

    private lateinit var readLogService: ReadLogService
    private var isBound = false
    private val permissions = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            isBound = false
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val readerBinder = service as ReadLogService.ReaderBinder
            readLogService = readerBinder.getService()
            isBound = true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            textView.text = savedInstanceState.getString(KEY_TEXT_VIEW)
        }

        val writeIntent = Intent(this, SaveLogService::class.java)

        addButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.add)}\"\n"
                )
                startService(writeIntent)
            }
        }

        deleteButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.delete)}\"\n"
                )
                startService(writeIntent)
            }
        }

        updateButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.update)}\"\n"
                )
                startService(writeIntent)
            }
        }

        createButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.create)}\"\n"
                )
                startService(writeIntent)
            }
        }

        putButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.put)}\"\n"
                )
                startService(writeIntent)
            }
        }

        readButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.read)}\"\n"
                )
                startService(writeIntent)
            }
        }

        logButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.log)}\"\n"
                )
                startService(writeIntent)
            }
        }

        openButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.open)}\"\n"
                )
                startService(writeIntent)
            }
        }

        saveButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.save)}\"\n"
                )
                startService(writeIntent)
            }
        }

        editButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.edit)}\"\n"
                )
                startService(writeIntent)
            }
        }

        uploadButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.update)}\"\n"
                )
                startService(writeIntent)
            }
        }

        loadButton.setOnClickListener {
            if (permissionResult() != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
            } else {
                writeIntent.putExtra(
                    KEY_LOG,
                    "${resources.getString(R.string.pressed)} \"${resources.getString(R.string.load)}\"\n"
                )
                startService(writeIntent)
            }
        }

        readLogsButton.setOnClickListener {
            textView.text = readLogService.readLogs()
        }

    }

    private fun permissionResult(): Int {
        return ContextCompat.checkSelfPermission(this, permissions[0])
    }


    override fun onStart() {
        super.onStart()
        val readIntent = Intent(this, ReadLogService::class.java)
        bindService(readIntent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        if (isBound) {
            unbindService(serviceConnection)
            isBound = false
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        this,
                        resources.getString(R.string.permission_notification),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_TEXT_VIEW, textView.text.toString())
    }
}
